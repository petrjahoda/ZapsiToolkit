﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ConfigJSON {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("JSON Config");
            Console.WriteLine("1. Install microsoft.extensions.configuration.json Nuget package");
            Console.WriteLine("1. Install newtonsoft.json Nuget package to write json to file");

            var configBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("config.json");
            var configuration = configBuilder.Build();
            var ipAddress = configuration["ipaddress"];
            var database = configuration["database"];

            Console.WriteLine("IP address: " + ipAddress);
            Console.WriteLine("Database: " + database);


            var currentDirectory = Directory.GetCurrentDirectory();
            const string configFile = "configuration.json";
            var outputPath = Path.Combine(currentDirectory, configFile);
            if (!File.Exists(outputPath)) {
                var configData = new List<Config> {
                    new Config() {
                        ipaddress = "192.168.0.1",
                        database = "zapsi2",
                        login = "zapsi_uzivatel",
                        password = "zapsi",
                        customer = "changeToCustomer"
                    }
                };
                var json = JsonConvert.SerializeObject(configData.ToArray());
                File.WriteAllText(outputPath, json);
            }
        }

        private class Config {
            public string ipaddress { get; set; }
            public string database { get; set; }
            public string login { get; set; }
            public string password { get; set; }
            public string customer { get; set; }
        }
    }
}