﻿using System;
using System.Diagnostics;
using System.Threading;

namespace FunTest{
    class Program{
        static void Main(string[] args){
            while (true){
                Stopwatch timer = Stopwatch.StartNew();
                Console.WriteLine("Processes running: " + Process.GetCurrentProcess().Threads.Count);
                Console.WriteLine(DateTime.Now.Millisecond);
                Thread.Sleep((int) (1000 - timer.ElapsedMilliseconds));
            }
        }
    }
}