﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Threads{
    class Program{
        static void Main(string[] args){

            Console.WriteLine("\tRunning threads/tasks beginning: " + Process.GetCurrentProcess().Threads.Count);
            Console.WriteLine("Threads");
//            for (int threadNumber = 0; threadNumber < 10; threadNumber++){
//                Console.WriteLine("\tRunning threads start: " + Process.GetCurrentProcess().Threads.Count);
//                Console.WriteLine("Running task " + threadNumber);
//                Thread thread = new Thread(() => Calculate(threadNumber, 10));
//                thread.Start();
//                Console.WriteLine("Closing task " + threadNumber);
//                Console.WriteLine("\tRunning threads end: " + Process.GetCurrentProcess().Threads.Count);
//            }

            Console.WriteLine("Tasks");
            for (int taskNumber = 0; taskNumber < 10; taskNumber++){
                Console.WriteLine("\tRunning tasks start: " + Process.GetCurrentProcess().Threads.Count);
                var task = Task.Run(() => { Calculate(taskNumber, 10); });
                task.Wait(); // wait to run the task before application ends
                Console.WriteLine("\tRunning tasks end: " + Process.GetCurrentProcess().Threads.Count);
            }
            Console.WriteLine("\tRunning threads/tasks ending: " + Process.GetCurrentProcess().Threads.Count);
        }


        private static int Calculate(int numberA, int numberB){
            var result = numberA + numberB;
            Console.WriteLine("Result is  " + result + " for task " + numberA);
            return result;
        }
    }
}