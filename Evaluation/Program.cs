﻿using System;
using System.Diagnostics;
using System.Globalization;
using Jace;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using NCalc;

namespace Evaluation{
    class Program{
        static void Main(string[] args){
            Console.WriteLine("Evaluation");
            Console.WriteLine("1. Install NCalc NuGet package");

            Expression e = null;
            if (e == null) {
                Console.WriteLine("NULL");
            } else {
            Console.WriteLine(e.Evaluate());
                
            }
            Expression f = new Expression("17<6 ");
            try {
                var result = f.Evaluate();
                Console.WriteLine(result);
            
            } catch (Exception exception) {
                Console.WriteLine(exception.Message);
                
            }

            Console.WriteLine(Convert.ToInt32("0"));
            Expression g = new Expression("Abs(-5)");
            Console.WriteLine(g.Evaluate());
            Expression h = new Expression("1 == 1 ||2 == 3");
            Console.WriteLine(h.Evaluate());
            var jahoda = "kofola";
            Console.WriteLine(jahoda.Replace(",","."));
            var now = DateTime.Now;
            var now2 = DateTime.Now;
            Console.WriteLine(now + "." + now.Millisecond);
            Console.WriteLine(now2 + "." + now2.Millisecond);
            
            
//            Console.WriteLine("START");
//            var sum = CSharpScript.EvaluateAsync("1 + 2").Result;
//            Console.WriteLine(sum.ToString());
//            Console.WriteLine("STOP");
//            sum = CSharpScript.EvaluateAsync("1 < 2").Result;
//            Console.WriteLine(sum.ToString());
//            Console.WriteLine("STOP");
//            sum = CSharpScript.EvaluateAsync("1 * 2.3").Result;
//            Console.WriteLine(sum.ToString());
         
            CalculationEngine engine = new CalculationEngine();
            Console.WriteLine(engine.Calculate("1 + 2"));
            Console.WriteLine(engine.Calculate("1 * 1.3"));
            Console.WriteLine(engine.Calculate("1 < 32"));
            Console.WriteLine(engine.Calculate("Abs (-5)"));

            
        }
    }
}