﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace WorkingWithFiles{
    class Program{
        static void Main(string[] args){
            Console.WriteLine("Working with files");
            // prepare ...
            var currentDirectory = Directory.GetCurrentDirectory();
            var dataFolder = "Data";
            var dataFile = "DataFile.txt";

            // create if not exist
            var outputPath = Path.Combine(currentDirectory, dataFolder, dataFile);
            Console.WriteLine(outputPath);
            var outputDirectory = Path.GetDirectoryName(outputPath);
            if (!Directory.Exists(outputDirectory)){
                Directory.CreateDirectory(outputDirectory);
            }

            // read data from file at once, if exist
            Console.WriteLine("\t FIRST READ");
            if (File.Exists(outputPath)){
                var readData = File.ReadAllText(outputPath);
                Console.WriteLine(readData);
            } else{
                Console.WriteLine("File not exists yet");
            }

            // write text to file at once(also create file if not exists)
            const string dataToWrite = "test data to write\n";
            File.WriteAllText(outputPath, dataToWrite);
            File.AppendAllText(outputPath, dataToWrite);

            Console.WriteLine("\t SECOND READ");
            if (File.Exists(outputPath)){
                var readData = File.ReadAllText(outputPath);
                Console.WriteLine(readData);
            } else{
                Console.WriteLine("File not exists yet");
            }

            // second method using file stream (for large files), reading and writing example
            using (var file = File.CreateText(outputPath)){
                file.WriteLine("Another text\nAnd another text");
            }

            Console.WriteLine("\t THIRD READ USING STREAM");
            if (File.Exists(outputPath)){
                using (var file = File.OpenText(outputPath)){
                    var text = file.ReadToEnd();
                    Console.WriteLine(text);
                }
            } else{
                Console.WriteLine("File not exists yet");
            }
        }
    }
}