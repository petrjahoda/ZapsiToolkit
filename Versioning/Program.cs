using System;
using System.IO;
using System.Threading;

namespace Versioning{
    class Program{
        static void Main(string[] args) {
            const string buildDate = "05/02/2018 21:19:29";
            Console.WriteLine("=== Versioning ===");
            Console.WriteLine("1. Create bat file and ps1 file inside the same directory");
            // add this to file:  (Get-Content Program.cs) | ForEach-Object { $_ -replace "const string buildDate = "05/02/2018 21:19:29";
            // add this to bat file powershell.exe -ExecutionPolicy Bypass -Command "./updateVersion.ps1"
            Console.WriteLine("2. Add this bat file as external tool before build, running befor build");
            Console.WriteLine("3. Build project");
            Console.WriteLine("Program built at: " + buildDate);
        }
    }
}
