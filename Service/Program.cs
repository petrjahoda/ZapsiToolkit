﻿using System;
using PeterKottas.DotNetCore.WindowsService;

namespace Service {
    class Program {
//            Linux Version http://pmcgrath.net/running-a-simple-dotnet-core-linux-daemon

//            Run the service without arguments and it runs like console app.
//            Run the service with action:install and it will install the service.
//            Run the service with action:uninstall and it will uninstall the service.
//            Run the service with action:start and it will start the service.
//            Run the service with action:stop and it will stop the service.
        static void Main(string[] args) {
            ServiceRunner<ZapsiService>.Run(config => {
                var name = "Test Service";
                config.Service(serviceConfig => {
                    serviceConfig.ServiceFactory((extraArguments, controller) => { return new ZapsiService(controller); });

                    serviceConfig.OnStart((service, extraParams) => {
                        Console.WriteLine("\tService {0} started", name);
                        service.Start();
                    });

                    serviceConfig.OnStop(service => {
                        Console.WriteLine("\tService {0} stopped", name);
                        service.Stop();
                    });

                    serviceConfig.OnError(e => { Console.WriteLine("\tService {0} errored with exception : {1}", name, e.Message); });
                });
            });
        }
    }
}