﻿using System;
using System.IO;
using System.Threading;
using PeterKottas.DotNetCore.WindowsService.Interfaces;

namespace Service{
    public class ZapsiService : IMicroService{
        private IMicroServiceController controller;

        public ZapsiService(){
            controller = null;
        }

        public ZapsiService(IMicroServiceController controller){
            this.controller = controller;
        }


        public void Start(){
            if (controller != null){
                controller.Stop();
            }

            // do something
        }

        public void Stop(){
            // do something at stop
        }
    }
}