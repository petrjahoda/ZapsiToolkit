﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Logging{
    class Program{
        static void Main(string[] args){
            Console.WriteLine("Logging");
            Console.WriteLine("1. Add Nuget microsoft.extensions.lgging");
            Console.WriteLine("1. Add Nuget microsoft.extensions.lgging.Console");
            Console.WriteLine("4. Add Nuget serilog.extensions.logging.file");


            // create directory and file forlogging, if not exists
            var currentDirectory = Directory.GetCurrentDirectory();
            var dataFolder = "Logs";
            var dataFile = "log.txt";
            var outputPath = Path.Combine(currentDirectory, dataFolder, dataFile);
            var outputDirectory = Path.GetDirectoryName(outputPath);
            if (!Directory.Exists(outputDirectory)){
                Directory.CreateDirectory(outputDirectory);
            }


            // do the logging
            var factory = new LoggerFactory();
            var logger = factory.CreateLogger("Main");
            factory.AddFile(outputPath, LogLevel.Trace);
            
            logger.LogCritical("Critical");
            logger.LogError("Error");
            logger.LogWarning("Warning");
            logger.LogInformation("Information");
            logger.LogDebug("Debug");
            logger.LogTrace("Trace");
            
            factory.Dispose();
            
            
            //  TAILILNG DATA: Get-Content .\log-20180504.txt -Last 10 -Wait | sls [INF]
        }
    }
}