﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.WebSockets;
using System.Runtime.ConstrainedExecution;
using System.Threading.Tasks;


namespace HttpDownload {
    class Program {
        static async Task Main(string[] args) {
            string filePath = "http://192.168.1.10/log/analog.txt";
            string fileSavePath = "c:\\Zapsi\\analog.txt";
            var timer = Stopwatch.StartNew();
            Console.WriteLine("Downloading started at " + DateTime.Now);

//            DownloadFile(filePath, fileSavePath);
            DeleteFile(filePath, fileSavePath);

            Console.Beep();
            Console.WriteLine("Time took: " + timer.Elapsed);
        }

        private static async Task DeleteFile(string filePath, string fileSavePath) {
            using (var client = new HttpClient())     
            {     
                var response = client.DeleteAsync(filePath).Result;     
                if (response.IsSuccessStatusCode)     
                {     
                    Console.Write("Success");     
                }     
                else     
                    Console.Write("Error");     
            }  
        }

        private static void DownloadFile(string filePath, string fileSavePath) {
            const int BUFFER_SIZE = 16 * 1024;
            using (var outputFileStream = File.Create(fileSavePath, BUFFER_SIZE)) {
                int i = 0;
                var req = WebRequest.Create(filePath);
                using (var response = req.GetResponse()) {
                    using (var responseStream = response.GetResponseStream()) {
//                        Console.WriteLine(responseStream.Length);
                        var buffer = new byte[BUFFER_SIZE];
                        int bytesRead;
                        do {
                            bytesRead = responseStream.Read(buffer, 0, BUFFER_SIZE);
                            outputFileStream.Write(buffer, 0, bytesRead);
                            if (i % 5 == 0) {
                                Console.Clear();
                            } else {
                                Console.Write("-");
                            }

                            i++;
                        } while (bytesRead > 0);
                    }
                }
            }
        }
    }
}