﻿using System;
using System.Net;
using System.Net.Mail;

namespace SendEmail {
    class Program {
        static void Main(string[] args) {
            SmtpClient client = new SmtpClient("smtp.forpsi.com");
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("support@zapsi.eu", "support01..");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("support@zapsi.eu");
            mailMessage.To.Add("petr.jahoda@me.com");
            mailMessage.Body = "body";
            mailMessage.Subject = "subject";
            client.Send(mailMessage);
        }
    }
}