﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SetDeviceTime {
    class Program {
        static void Main(string[] args) {
            var server = "192.168.1.10";
            var sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,ProtocolType.Udp);
            var serverAddr = IPAddress.Parse(server);
            var endPoint = new IPEndPoint(serverAddr, 9999);
            var dateNow = DateTime.Now;
            var date = String.Format("{0:dd.MM.yyyy HH:mm:ss}", dateNow);
            var dayOfWeek = (int) dateNow.DayOfWeek;
            var message = "set_datetime=" + date + " 0" + dayOfWeek + "&";
            Console.WriteLine(message);
            var send_buffer = Encoding.ASCII.GetBytes(message);
            sock.SendTo(send_buffer, endPoint);
        }
    }
}