﻿using System;
using System.Diagnostics;
using System.Threading;
using Hangfire;
using Hangfire.MemoryStorage;

namespace Timer {
    class Program {
        static void Main(string[] args) {
            // version one
            int cycleTimeInMilliseconds = 1000;
            var timer = new System.Timers.Timer(cycleTimeInMilliseconds);
            Console.WriteLine("Service started, press CTRL+C to end the program");
            timer.Elapsed += (sender, e) => {
                Console.Write(DateTime.Now.Millisecond + " ");
                Console.WriteLine("Checking MySQL Connection, processes: " +
                                  Process.GetCurrentProcess().Threads.Count);
            };
            timer.Start();
            while (timer.Enabled) {
                Thread.Sleep(cycleTimeInMilliseconds);
            }

            timer.Stop();
            timer.Dispose();

            // version two
//                GlobalConfiguration.Configuration.UseMemoryStorage();
//                RecurringJob.AddOrUpdate(() => Console.WriteLine("JOB"), Cron.Minutely);

//                using (var server = new BackgroundJobServer()){
//                    Console.WriteLine("Started");
//                    Console.ReadKey();
//                }
        }
    }
}