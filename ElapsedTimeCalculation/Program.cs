﻿using System;
using System.Diagnostics;
using System.Threading;

namespace ElapsedTimeCalculation{
    class Program{
        static void Main(string[] args){
            Console.WriteLine("Elapsed Time Calculation");
            Stopwatch timer = Stopwatch.StartNew();
            Thread.Sleep(900);
            Console.WriteLine("Took time " + timer.Elapsed);
            timer.Restart();
            Thread.Sleep(500);
            Console.WriteLine("Took time " + timer.Elapsed);
            timer.Stop();
        }
    }
}