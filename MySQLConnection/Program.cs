﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace MySQLConnection{
    class Program{
        static void Main(string[] args){
            Console.WriteLine("MySQL Connection");
            Console.WriteLine("!. install mysql.connector NuGet package");


            var connection = new MySqlConnection("server=localhost;userid=root;password=54321;database=zapsi2;");
            Console.WriteLine("Connecting using " + connection.ConnectionString);
            try{
                connection.Open();
            } catch (Exception e){
                throw e;
                Console.WriteLine(e.Message);
            }

            if (connection.State.Equals(ConnectionState.Open)){
                Console.WriteLine(connection.State);
                MySqlCommand command = new MySqlCommand("SELECT * FROM zapsi2.device;", connection);
                using (MySqlDataReader reader = command.ExecuteReader()){
                    System.Console.WriteLine("OID\t\tCustomerID\t\tHardwareID");
                    while (reader.Read()){
                        string row = $"{reader["OID"]}\t\t{reader["CustomerID"]}\t\t{reader["HardwareID"]}";
                        Console.WriteLine(row);
                    }
                }

                connection.Close();
            }

            Console.WriteLine(connection.State);
        }
    }
}